#!/bin/python

import config
import json
import os, telebot
import requests
import re
bot = telebot.TeleBot(config.BOT_TOKEN)

@bot.message_handler(commands=['start', 'cool-man'])
def welcome(message):
    bot.reply_to(message, "Wtf bro wat u want?")

@bot.message_handler(commands=['movieapi'])
def movieapi(message):
    param = matches = re.findall(r'"([^"]*)"', message.text)
    response = requests.get(f"https://sg.media-imdb.com/suggests/f/{param}.json")
    if response.status_code == 200:
        jsonp_response = response.text
        json_data_str = jsonp_response.split('(', 1)[1].rstrip(')')
        json_data = json.loads(json_data_str)
        xid = json_data['d'][0]['id']
        bot.reply_to(message, f"https://tenderbolt-studios.github.io/Movie-Api-test/v1/?id={cid}&s=&e=")
    else:
        print(f"Error: {response.status_code}")
        bot.reply_to(message, 'Wtf bro wat u want? what you entered is wrong.use `/movieapi "name"`')

@bot.message_handler(commands=['stkrElement'])
def stickerToElement(message):
    bot.send_message(message.chat.id, "Attempt started")
    print(message.text)
    head = message.text.find("addstickers/")
    if head == -1:
        bot.send_message(message.chat.id, "Send correct sticker link u idiot")
        return 0
    s = ""
    for i in range(len(message.text)-head):
        s += message.text[head+i]
    sticklink = "https://t.me/" + s
    if os.path.isfile("stickerpack.git.lock"):
        bot.reply_to(message, "Looks like another pack is currently being imported. Send link later.")
        return 0
    os.system("touch stickerpack.git.lock")
    os.system("/data/data/com.termux/files/home/myScripts/addStickerToMatrix.sh "+sticklink)
    bot.reply_to(message, "Hopefully, "+s+" will be added to Matrix...(Soon™)")
    os.remove("stickerpack.git.lock")

@bot.message_handler(commands=['instadl'])
def instadl(message):
    bot.send_message(message.chat.id, "Attempt started")
    #os.chdir("instadl")
    os.system("yt-dlp "+ message.text.replace("/instadl ", "") + " -o instadl/insta.mp4")
    print(message.text)
    #os.chdir("..")
    bot.send_video(message.chat.id, telebot.types.InputFile("/data/data/com.termux/files/home/myProjects/tgbot/instadl/insta.mp4"))

@bot.message_handler(func=lambda msg: True)
def echoing(message):
    bot.reply_to(message, message.text)
def app():
    bot.infinity_polling()
